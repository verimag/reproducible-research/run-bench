type prog_par = string list
(* to run a program, several parameters can be necessary (name, dir, etc.).  *)

type cmd = string (* a shell call with parameters ($1, $2, etc.). Meant to be called on program pameters *)
type name = string
type ci_rel_size = float option
type phase = name * cmd list * ci_rel_size
type job = name * phase list (* source level job *)

(* XXX les noms sont pas terribles (job vs jobs !!!) *)

type phase2run = (string * string * float option) * string
  (* (phase name, script name, ci rel size), script args *)
type job2run = string * string * (phase2run list)
  (* program name, job name, phases to be run in sequence *)
type jobs2run = job2run list (* jobs to be run in parallel *)
