

  $ echo "label1 p1.c\nlabel2 p2.c\nlabel1 p3.c\n" > /tmp/some_bench.txt

  $ echo "a_stupid_job:\n  - a_stupid_phase\n     - echo $1 $2\n" > /tmp/some_job.yml

  $ run-bench -j /tmp/some_job.yml /tmp/some_bench.txt
  The first labels in /tmp/some_bench.txt should be unique. Duplicated labels are: label1
  [2]
