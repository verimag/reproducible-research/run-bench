(* Time-stamp: <modified the 10/01/2024 (at 14:47) by Erwan Jahier> *)

open Types

let (string_to_string_list : string -> string list) =
  fun str ->
  if String.length str > 0 && str.[0] = '#' then [] else
    Str.split (Str.regexp "[ \t]+") str

let (duplicates: string list -> string list) =
  fun l ->
  let rec aux acc dupl l =
    match l with
    | [] -> dupl
    | lbl::tail ->
      if List.mem lbl acc then
        aux acc (lbl::dupl) tail
      else
        aux (lbl::acc) (dupl) tail
  in
  aux [] [] l

let _ =
  assert (duplicates ["a";"b";"c"; "d"] = []);
  assert (duplicates ["a";"b";"c"; "d"; "d"; "a"] = ["a"; "d"])


let (f : string -> prog_par list) =
  fun fn ->
  let ic = open_in fn in
  let rec read acc =
    try read ((input_line ic |> string_to_string_list) :: acc)
    with _ -> acc
  in
  let res = read [] |> List.filter (fun l -> l <> []) |> List.rev in
  let dupl_labels = List.map List.hd res |> duplicates in
  if dupl_labels <> []then (
    Printf.printf "The first labels in %s should be unique. Duplicated labels are: %s\n%!"
      fn (String.concat "," dupl_labels);
    exit 2);
  res
