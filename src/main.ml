(* Time-stamp: <modified the 15/03/2024 (at 09:54) by Erwan Jahier> *)
(*-----------------------------------------------------------------------
** This file may only be copied under the terms of the CeCill
** Public License
**-----------------------------------------------------------------------
**
** Author: erwan.jahier@univ-grenoble-alpes.fr
**
*)

open MainArgs

let version = "1.0"
let sha = ""


(******************************************************************************)
(* XXX a ranger ailleurs *)

open Types

(* Generate an sh script body out of a phase definition *)
let (phase_to_script : cmd list -> string) =
  fun cmds ->
  Printf.sprintf "%s\n%s\n%s\nworkdir=%s\n%s\n" !MainArgs.shell
    (Utils.entete "#" version sha)
    (if !verbose then "set -x" else "")
    !work
    (String.concat "\n" cmds)


(* perform the  cartesian product of commands (coming  from jobs) and
   program  arguments (contained  in =prog_par=)  .  The  idea is  to
   return a  list of sys call  that can be done  in parallel (namely,
   jobs working on different prog_pars) *)


let (make_script_name: string -> string -> string) =
  fun job pname ->
  Filename.concat !work (job ^ "-" ^ pname ^ ".sh")

let (make_jobs : prog_par list -> job list -> jobs2run) =
  fun prog_pars jobs ->
  let args = List.map (fun prog -> String.concat " " prog) prog_pars in
  let labels = List.map List.hd prog_pars in
    List.map (fun (job, phases) ->
        let phase_to_run_list: (string * string * (float option)) list =
          List.map
            (fun (pname, cmds, ci_rl) ->
               let script_name = make_script_name job pname in
               let script_body = phase_to_script cmds in
               Utils.gen_file Open_trunc script_name script_name script_body;
               pname, script_name, ci_rl)
            phases
        in
        Utils.list_product phase_to_run_list args |>
        List.map2 (fun label l -> (label ,job, l) ) labels
      )
      jobs
    |> List.flatten

let mkdir_if_needed d = if (not (Sys.file_exists d)) then Sys.mkdir d 0o766

let (make_org_link: string * string list list -> string list -> string * string list list) =
  fun (job,acc) l ->
  match l with
  | [job] ->
    (job , [job]::acc)
  | [s0;s1;s2;s3] ->
    assert (Filename.check_suffix s2 ".log");
    job, [
      (*       Printf.sprintf "[[file:%s][%s]]" (make_script_name job s0) s0; *)
      Printf.sprintf "[[file:%s][%s]]" (make_script_name job s0) s1;
      Printf.sprintf "[[file:%s][%s]]" s2 s3]::acc
  | _ ->
    Printf.printf "Error? make_org_link %d\n%!" (List.length l);
     (job,acc)

let (remove_log: string list list -> string list -> string list list) =
  fun acc l ->
  match l with
  | [job] ->
    [job]::acc
  | [_s0;s1;s2;s3] ->
    assert (Filename.check_suffix s2 ".log");
    [s1;s3]::acc
  | _ ->
    Printf.printf "Error? make_org_link %d\n%!" (List.length l);
     acc


(* open Functory.Sequential *)
open Functory.Cores


let main () =
  if Array.length Sys.argv <= 1 then (Arg.usage speclist usage; flush stdout; exit 2)
  else (
    try
      if !verbose then (Printf.printf "Parsing CLI\n%!");
      Arg.parse speclist (fun s -> bench_file := s) usage
    with
    | Failure(e) -> print_string ("run-bench: "^e); flush stdout; flush stderr; exit 2
    | e -> print_string ("run-bench: "^(Printexc.to_string e));  flush stdout; exit 2
  );
  Functory.Cores.set_number_of_cores !cores_nb;
  List.iter mkdir_if_needed [!work; !log; !res];
  if !verbose then (Printf.printf "Parse programs (.txt file)\n%!");
  let prog_par = ParsePrograms.f !bench_file in
  if !verbose then (Printf.printf "Parse jobs (.yml file)\n%!");
  let src_jobs = ParseJobs.f !jobs_file in
  if !verbose then (Printf.printf "Parse jobs done\n%!");
  let jobs: jobs2run = make_jobs prog_par src_jobs in
  let column_names : string list =
    jobs |> List.hd |>
    (fun (_pgn,_jn,pl) ->
       ["program";"job"]::
       (List.map (fun ((pn,_,_),_) -> [pn^"-count";pn^"-times"]) pl))
    |>
    List.flatten
  in
  (* debug *)
  if !verbose then (
    Printf.printf "%s\n" (ParseJobs.to_string src_jobs);
    Printf.printf "%s\n" (String.concat "\n" (List.map (String.concat " ") prog_par));
    jobs |>
    List.map
      (fun (pgn, jn, l) ->
         "\n" ^ pgn ^ "/" ^ jn ^ ": " ^ (String.concat "" (List.map (fun ((pn, sn, _), args) ->
             Printf.sprintf "[%s] %s %s;   " pn sn args) l))
      )
    |> (String.concat "\n") |> Printf.printf "\nmake_it:\n%s\nmake_it_end\n%!";
  );
  let prog_par_tbl = Hashtbl.create 100 in
  List.iter (function | x::tail -> Hashtbl.add prog_par_tbl x tail | _ -> assert false)
    prog_par;
  let add_prog_par = function
    | x::tail -> x::tail @ (Hashtbl.find prog_par_tbl x)
    | _ -> assert false
  in
  (*   let result0 = List.map Run.run_job jobs in *)

  (match !org with
   | Some f ->
     String.concat "|" column_names |> (Printf.sprintf "|%s|\n|-|") |>
     Utils.gen_file Open_append f ""
   | None -> ()
  );
  (match !raw with
   | Some f ->
     String.concat " " column_names |> (Printf.sprintf "# %s") |>
     Utils.gen_file Open_append f ""
   | None -> ()
  );
  (match !csv with
   | Some f ->
     String.concat "," column_names |> (Printf.sprintf "# %s") |>
     Utils.gen_file Open_append f ""
   | None -> ()
  );

  let _result =
    let zefold (_acc:name list list list) (res:name list list) =
      let label = match res with
        | program::job::_ -> (List.hd program) ^ "/" ^ (List.hd job)
        | _ -> assert false
      in
      (match !org with
       | Some f ->
         res |>
         (List.fold_left make_org_link ("",[])) |> snd |> List.rev |>
         List.flatten |> (String.concat "|") |>
         (Printf.sprintf "|%s|%!") |>
         Utils.gen_file Open_append f label
       | None -> ()
      );
      (match !raw with
       | Some f ->
         res |>
         (List.fold_left remove_log []) |> List.rev |> List.flatten |>
         add_prog_par |> (String.concat " ") |> (Printf.sprintf "%s%!") |>
         Utils.gen_file Open_append f label
       | None -> ()
      );
      (match !csv with
       | Some f ->
         res |>
         (List.fold_left remove_log []) |> List.rev |> List.flatten |>
         add_prog_par |> (String.concat ",") |> (Printf.sprintf "%s%!") |>
         Utils.gen_file Open_append f label
       | None -> ()
      );
      []
    in
    map_local_fold ~f:Run.run_job ~fold:zefold [] jobs
  in

  Printf.printf "cf %s/ for log files; and %s/ for data files\n%!" !log !res;

  ()
let _ = main ()
