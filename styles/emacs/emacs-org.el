

(setq load-path (cons (expand-file-name "./el") load-path))
(setq load-path (cons (expand-file-name "./") load-path))


(setq auto-mode-alist (cons '("\\.lut$" . lutin-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.lus$" . lustre-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.rif$" . rif-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.ml$" . tuareg-mode) auto-mode-alist))


(autoload 'rif-mode "rif" "" t)
(autoload 'lutin-mode "lutin" "Edition de code lutin" t)
(autoload 'lustre-mode "lustre" "Edition de code lustre" t)

;;(autoload 'tuareg-mode "ocaml" "" t)


(require 'org)

(require 'htmlize)
(require 'ob)
(require 'ob-dot)
;(require 'ob-yml)
(require 'ob-ocaml)
(require 'zenburn)


;; make sure that when we export in HTML, that we don't export with inline css.
;; that way the CSS of the HTML theme will be used instead which is better
(setq org-html-htmlize-output-type 'css)

(setq org-src-fontify-natively t)
(setq org-src-preserve-indentation t)

;(setq  org-confirm-babel-evaluate t)

(defun my-org-confirm-babel-evaluate (lang body)
  (and (not (string= lang "dot")) (not (string= lang "sh"))))
(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)

;(mapcar #'cdr (org-babel--get-vars params))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (ocaml . t)
   (dot . t)
   (lustre . t)
   (lutin . t)
   (dot . t)
 ;  (yml . t)
   (rif . t)
   (shell . t)
   )
 )

;;; pour faire comme suggeré ici
;;; https://lists.gnu.org/archive/html/emacs-orgmode/2011-03/msg00857.html
;;; mais ca ne fonctionne pas (avec cette vrsion de org-mode)
;; (add-hook 'org-babel-tangle-body-hook
;;             (lambda () (org-export-preprocess-apply-macros)))
