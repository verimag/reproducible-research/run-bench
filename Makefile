


build:
	dune build @install

-include ./Makefile.version


install: doc qtest
	dune install

uninstall:
	dune uninstall

clean:
	dune clean

qtest:
	dune runtest

test:
	cd expe; make test
utest:
	dune promote

###################################################################
doc: README.html

EMACS=emacs --batch \
	--eval="(add-to-list 'load-path \"./styles/emacs\") (add-to-list 'load-path \"./styles/emacs\")" \
	--load=emacs-org.el

%.html:%.org
	$(EMACS) --visit=$*.org --funcall org-html-export-to-html

%.md:%.org
	$(EMACS)  --visit=$*.org --funcall org-md-export-to-markdown
