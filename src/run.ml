(* Time-stamp: <modified the 12/02/2024 (at 11:21) by Erwan Jahier> *)

open Types


(**********************************************************************)
(* run a shell command and get all its output in a list (stderr+stdout) *)
let (run0 : string -> (string -> string option) -> string list) =
 fun cmd filter ->
  let proc = Unix.open_process_in ("("^cmd^" ) 2>&1") in
  let list = ref [] in
  try
    while true do
      let line = input_line proc in
        match filter line with
          | None -> ()
          | Some str -> list := str::!list
    done;
    []
  with End_of_file ->
    ignore (Unix.close_process_in proc);
    List.rev !list

(* get the command outputs in various forms *)
let (run : string -> string list) =
  fun str -> run0 str (fun s -> Some s) (* filters nothing *)
let (run_str : string -> string)  = fun str -> String.concat "\n" (run str)
let (run_unit : string -> unit) = fun str -> print_string (run_str str)
let sh = run_unit

let time f x =
  let t = Sys.time() in
  let fx = f x in
  Printf.printf "Execution time: %fs\n" (Sys.time() -. t);
  fx

let (pt_diff : Unix.process_times -> Unix.process_times -> float) = fun pt2 pt1 ->
  (pt2.tms_cutime +. pt2.tms_cstime -. pt1.tms_cutime  -. pt1.tms_cstime)

(* to hold the exec time of various runs *)
type t = {  all: float list;  sum: float;  mini: float;  maxi: float }
let init = { all = []; sum = 0.0; mini = max_float; maxi = 0.0 }

let update v x =  {
  all = v::x.all;
  sum = v +. x.sum;
  mini = min v x.mini;
  maxi = max v x.maxi;
}

let tf = float_of_int
let stddev m xl =
  let n = List.length xl in
  let sum =
    List.fold_left (fun acc xi -> acc +. (m -. xi) *. (m -. xi)) 0.0 xl
  in
  sqrt (sum /. (tf n))

(**********************************************************************)

let (one : prog_par -> cmd -> string) =
  fun prog_par cmd  ->
  (* XXX remplacer les $1 $2 etc  dans cmd avec le contenu de prog_par *)

  Printf.sprintf "%s %s\n" (String.concat "" prog_par) cmd

let (ps2str: Unix.process_status -> string) =
  fun ps ->
  (match ps with
   | Unix.WEXITED i -> Printf.sprintf "exit(%d)" i
   | Unix.WSIGNALED i -> Printf.sprintf "kill(%d)" i
   | Unix.WSTOPPED i -> Printf.sprintf "stop(%d)" i)

exception Pb of string list

let to_str = Printf.sprintf "%.5f"

let run_simus log threshold min_simu_nb max_simu_nb timeout cmd =
  let t = Unix.times() in
  let measures = ref init in
  let continue = ref true in
  let oc = open_out_gen [Open_wronly; Open_creat; Open_append] 0o666 log in
  let continue_update i m stddev =
    let x = (1.96 *. stddev /. (threshold *. m)) ** 2.0 in
    let y = (1.96 *. stddev *. 10.) ** 2.0 in
    let ci_size = 2.0 *.  1.96 *. stddev *. stddev /. (sqrt (tf i)) in
    Printf.fprintf oc "we continue as long as |CI|=%f > m.rho=%f or %d<10\n%!"
      ci_size (threshold *. m) i;
    Printf.fprintf oc "m=%f (stddev=%f) > %f or %f ? \n%!" m stddev x y;
    continue := i < max_simu_nb && ( (* do at most max_simu_nb simulations *)
      (pt_diff (Unix.times()) t < timeout) &&
      (i < min_simu_nb
       ||
       ( ci_size > threshold *. m)
       ))
  in
  let i = ref 0 in
  while !continue  do
    let pt0 = Unix.times () in
    let res = Unix.system cmd in
    if res <> WEXITED 0 then
      raise (Pb [Printf.sprintf "the sys call '%s' failed (%s)"  cmd (ps2str res); log; "-"]);
    let pt1 = Unix.times () in
    let wct = pt_diff pt1 pt0  in
    measures := update wct !measures;
    incr i;
    Printf.fprintf oc "\n%i: time = %f mean = %.2f\n%!" !i wct (!measures.sum /. (tf !i));

    let wct_mean = (!measures.sum /. (tf !i)) in
    let wct_stddev = stddev wct_mean !measures.all in
    continue_update !i wct_mean wct_stddev
  done;
  let wct_mean = (!measures.sum /. (tf !i)) in
  let wct_stddev = stddev wct_mean !measures.all in

  let ci_size = 2.0 *.  1.96 *. wct_stddev *. wct_stddev /. (sqrt (tf !i)) in
  let wct_str = Printf.sprintf "\"[%.2f ... %.2f~%.2f ... %.2f]   |CI_95|=%.2f n=%d\"%!"
      !measures.mini wct_mean wct_stddev !measures.maxi ci_size !i
  in

  Printf.fprintf oc "Total Execution time (user+sys): %fs\nSummary: %s\nLoad: %s\n%!"
    (pt_diff (Unix.times()) t) wct_str (run_str "cat /proc/loadavg");
  if !i = max_simu_nb then
    Printf.printf "ZZZ Maximal number of simulations (%d) reached!\n" max_simu_nb;
  if pt_diff (Unix.times()) t > timeout  then
    Printf.printf "ZZZ Timeout! (%f > %f)\n"  (pt_diff (Unix.times()) t) timeout ;
  close_out oc;
  [string_of_int !i; log; to_str wct_mean]

(**********************************************************************)


type phase_result = Error of string * int | Ok of string


let (run_phase: string -> string -> phase2run -> string list) =
  fun pgn jn ((pn, sn, ci_rel_size), args) ->
  let log = Filename.temp_file ~temp_dir:!MainArgs.log (pgn^"__"^jn^"_"^pn^"_") ".log" in
  (*   let cmd = Printf.sprintf "%s %s &>%s" sn args log in *)
  (*   ignore (Unix.system (Printf.sprintf "rm -f %s; touch %s" log log)); *)
  let cmd = Printf.sprintf
      "   %s %s  >> %s 2>&1"
      sn args log
  in
  ignore (Unix.system (Printf.sprintf "echo \"This file contains the output of the shell command:\n%s\" > %s\n" cmd log));
    if !MainArgs.verbose then Printf.printf "%s\n%!" cmd;
  match ci_rel_size with
  | None ->
    let pt0 = Unix.times () in
    let res = Unix.system cmd in
    if res <> WEXITED 0 then
      raise (Pb [pn;Printf.sprintf "%s" (ps2str res); log; "-"]);
    let pt1 = Unix.times () in

    ignore (Unix.system (
        Printf.sprintf  "echo \"Total Execution time (user+sys): %fs\nLoad: %s\n\" >> %s\n"
          (pt_diff pt1 pt0) (run_str "cat /proc/loadavg") log
      ));
    [pn; "1"; log; to_str(pt_diff pt1 pt0)]
  | Some ci ->
    let res =
      (* Printf.printf "Running %s of job %s (cf %s)\n%!" pn jn log; *)
      run_simus log ci !MainArgs.min_cmd_nb !MainArgs.max_cmd_nb !MainArgs.timeouts cmd
    in
    Printf.printf "a phase %s of job %s finished, cf %s\n%!" pn jn log;
    pn::res

(* like map, but stop at the first error *)
let map_stop f l =
  let rec aux acc l =
  match l with
    | [] -> List.rev acc
    | x::t ->
      try aux ((f x)::acc) t
      with Pb l -> List.rev_append acc [l]
  in
  aux [] l

let (run_job: job2run -> string list list) =
  fun (pgn, jn, phases) ->
  let res = map_stop (run_phase pgn jn) phases in
  [pgn]::[jn]::res
