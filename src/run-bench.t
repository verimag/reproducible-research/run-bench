Test the -help option

  $ run-bench -h
  run-bench [options]* -j <jobs>.yml <benchs>
  where
    <jobs>.yml yaml file with naming conventions
    <benchs> is a file made of a list of space-separated strings.
  
  More information on https://gricad-gitlab.univ-grenoble-alpes.fr/Reproducible-Research/run-bench
  
    --jobs-file <string>.yml
    -j <string>.yml	 set the jobs file name (default is jobs.yml)
    --cores-nb <int>
    -n <int>		 set the max number of jobs to run in parallel (default is 1)
    --timeouts <float>
    -t <float>		 (in sec) set jobs timeout (default is 360.00)
    --min-run-nb <int>
    -min <int>		 Minimum number of run for a command (default is 10)
    --max-run-nb <int>
    -max <int>		 Maximum number of run for a command (default is 1000)
    --log-dir <string>
    -log <string>	 set where .log files are generated (default is $TESTCASE_ROOT/log)
    --work-dir <string>
    -work <string>	 set where the experiments is run (default is $TESTCASE_ROOT/work)
    --res-dir <string>
    -res <string>	 set where the results are generated (default is $TESTCASE_ROOT/res)
    -org 		 generate the result of each run a .org file
    -csv 		 generate the result of each run a .csv file
    -raw 		 generate the result of each run a .raw file
    --verbose  
    -verbose  
    -v 		 set on a verbose mode
    -h 		 display this help message
