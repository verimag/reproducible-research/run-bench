(* Time-stamp: <modified the 23/05/2024 (at 15:45) by Erwan Jahier> *)
let usage = "run-bench [options]* -j <jobs>.yml <benchs>
where
  <jobs>.yml yaml file with naming conventions
  <benchs> is a file made of a list of space-separated strings.

More information on https://gricad-gitlab.univ-grenoble-alpes.fr/Reproducible-Research/run-bench
"

let usage_out speclist errmsg =
  Printf.printf "%s" (Arg.usage_string speclist errmsg)

let jobs_file = ref "jobs.yml"
let bench_file = ref "benchs"
let cores_nb = ref 1
let timeouts = ref 360.
let min_cmd_nb = ref 10
let max_cmd_nb = ref 1000
let log = ref (Filename.concat (Sys.getcwd()) "log")
let work = ref (Filename.concat (Sys.getcwd()) "work")
let res = ref (Filename.concat (Sys.getcwd()) "res")

let gmt = Unix.gmtime (Unix.time())
let gmt_str = Printf.sprintf "%d-%d-%d_%d-%d-%d"
    gmt.tm_mday gmt.tm_mon (1900+gmt.tm_year) gmt.tm_hour gmt.tm_min gmt.tm_sec
let org = ref (Some (Filename.concat !res (Printf.sprintf "result-%s.org" gmt_str )))
let csv = ref (Some (Filename.concat !res (Printf.sprintf "result-%s.csv" gmt_str )))
let raw = ref (Some (Filename.concat !res (Printf.sprintf "result-%s.raw" gmt_str )))
let verbose = ref false
let shell = ref "#!/bin/bash"

let rec speclist =
  [
    "--jobs-file", Arg.String (fun f -> (jobs_file := f)),"<string>.yml";
    "-j", Arg.String (fun f -> (jobs_file := f)),
    (Printf.sprintf "<string>.yml\t set the jobs file name (default is %s)" !jobs_file);

    "--cores-nb", Arg.Int (fun i -> (cores_nb := i)),"<int>";
    "-n", Arg.Int (fun i -> (cores_nb := i)),
    (Printf.sprintf "<int>\t\t set the max number of jobs to run in parallel (default is %d)" !cores_nb);

    "--timeouts", Arg.Float (fun i -> (timeouts := i)),"<float>";
    "-t", Arg.Float (fun i -> (timeouts := i)),
    (Printf.sprintf "<float>\t\t (in sec) set jobs timeout (default is %.2f)" !timeouts);

    "--min-run-nb", Arg.Int (fun i -> (min_cmd_nb := i)),"<int>";
    "-min", Arg.Int (fun i -> (min_cmd_nb := i)),
    (Printf.sprintf "<int>\t\t Minimum number of run for a command (default is %d)" !min_cmd_nb);

    "--max-run-nb", Arg.Int (fun i -> (max_cmd_nb := i)),"<int>";
    "-max", Arg.Int (fun i -> (max_cmd_nb := i)),
    (Printf.sprintf "<int>\t\t Maximum number of run for a command (default is %d)" !max_cmd_nb);

    "--log-dir",Arg.String (fun str -> (log := str)),"<string>";
    "-log",Arg.String (fun str -> (log := str)),
    (Printf.sprintf "<string>\t set where .log files are generated (default is %s)" !log);

    "--work-dir",Arg.String (fun str -> (work := str)),"<string>";
    "-work",Arg.String (fun str -> (work := str)),
    (Printf.sprintf "<string>\t set where the experiments is run (default is %s)" !work);

    "--res-dir",Arg.String (fun str -> (res := str)),"<string>";
    "-res",Arg.String (fun str -> (res := str)),
    (Printf.sprintf "<string>\t set where the results are generated (default is %s)" !res);

    "-org",Arg.String (fun str -> (org := Some str)),
    "\t\t generate the result of each run a .org file";
    "-csv",Arg.String (fun str -> (csv := Some str)),
    "\t\t generate the result of each run a .csv file";
    "-raw",Arg.String (fun str -> (raw := Some str)),
    "\t\t generate the result of each run a .raw file";

    "--verbose",Arg.Unit (fun _ -> (verbose := true)), " ";
    "-verbose",Arg.Unit (fun _ -> (verbose := true)),  " ";
    "-v",Arg.Unit (fun _ -> (verbose := true)),    "\t\t set on a verbose mode";

    "--help", Arg.Unit (fun _ -> (usage_out speclist usage ; exit 0)), "";
    "-help", Arg.Unit (fun _ -> (usage_out speclist usage ; exit 0)),  "";
    "-h", Arg.Unit (fun _ -> (usage_out speclist usage ; exit 0)),
    "\t\t display this help message"
  ]
